# Cli CredTrans

This is a CLI for Validate Transactions, write in python with Click.

[![Python Version][python-image]][python-url]
[![Click Version][click-image]][click-url]

## Motivation

The creation of this software was generated based on the particular interest in studying command line applications. So we discovered in Click a potential tool, to allow the creation, speed and practicality in the development of a small application for this purpose. This software read file with client information, and return validation to all transactions.

## Code Style

[![code-style]][code-url]
[![pattern]][pattern-url]

For programming in this project, using python, we recommend the quick reading of the coding style based on PEP 8:

### Style

* [PEP 8](https://www.python.org/dev/peps/pep-0008/)

### Linters and Formatters

* [Black][code-url]

### Code Patterns

* [Snake Case](https://en.wikipedia.org/wiki/Snake_case)

## Features

* [Docker](https://docs.docker.com/)
* [Click](https://click.palletsprojects.com/en/7.x/)
* [Python](https://www.python.org/)

## Prerequisites

Below is the list of packages and modules used in the project.

Package                                      | Version  |
---------------------------------------------| ---------|
[Click][click-url]                           | 7.1.1    |
[dateutil][dateutil-url]                     | 2.8.1    |


## Usage

There are two ways to carry out the project, follow the suggestions.

### Local Instance

Very simple, just create a python environment, in version 3.7 and finally install the project according to the commands below.

#### Installation

```
$ virtualenv venv
$ . venv/bin/activate
$ pip install --editable .
```

#### Execute

```
$ authorize -f /path/to/file
```

### Container Instance

In the project root folder execute the following commands:

#### Installation

```
docker build -t credtrans .
```

#### Execute

```
docker run --rm -it credtrans ./examples/operations
```

## Meta

Distributed under the MIT license. See ``LICENSE`` for more details.

## Contribute

1. Fork it (<https://gitlab.com/xarpy/cli_credtrans>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[python-image]: https://img.shields.io/badge/python-v3.7.3-blue
[click-image]: https://img.shields.io/badge/click-v7.1.1-blue
[dateutil-image]: https://img.shields.io/badge/dateutil-v2.8.1-blue
[python-url]: https://www.python.org/downloads/release/python-374/
[click-url]: https://click.palletsprojects.com/en/7.x/
[dateutil-url]: https://dateutil.readthedocs.io/en/stable/
[pattern]:https://img.shields.io/badge/code%20style-snake__case-green
[pattern-url]:https://en.wikipedia.org/wiki/Snake_case
[code-url]:https://github.com/psf/black
[code-style]:https://img.shields.io/badge/code%20style-black-black