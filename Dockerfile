FROM python:3-alpine

RUN mkdir /credtrans

WORKDIR "/credtrans"

COPY . /credtrans

RUN mkdir /credtrans/upload

VOLUME [ "/credtrans/upload" ]

RUN pip install --editable .

ENTRYPOINT ["authorize","-f"]