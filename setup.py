# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

__version__ = "0.1.0"
__description__ = "CLI CredTrans"
__long_description__ = "This is an simple CLI for validate transactions"

__author__ = "Xarpy"

setup(
    name="credtrans",
    version=__version__,
    author=__author__,
    packages=find_packages(),
    license="MIT",
    description=__description__,
    long_description=__long_description__,
    url="https://gitlab.com/xarpy/cli_credtrans",
    keywords="CLI, Python, CLick",
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Operating System :: OS Independent",
        "Topic :: Software Development",
        "Environment :: CLI Environment",
        "Programming Language :: Python :: 3.7",
        "License :: MIT License",
    ],
    install_requires=["Click", "python-dateutil"],
    entry_points="""
        [console_scripts]
        authorize=commands.operation:authorize
    """,
)
