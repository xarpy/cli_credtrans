# -*- coding: utf-8 -*-

import os
import click
from utils import check_all_transactions, convert_input


@click.command()
@click.option(
    "--file",
    "-f",
    type=click.Path(),
    default="~/operations",
    help="Input the file with all data for transactions.",
)
def authorize(file):
    """
    This is a CLI for Validate Transactions 🏦

    A small tool, to increase the practicality of using validations,
    before attempting to record the information in the database.
    Is used for credit transactions.
    """
    try:
        filename = os.path.expanduser(file)
        if os.path.exists(filename):
            with open(filename, "r") as bigfile:
                text = convert_input(bigfile.readlines())
                result = check_all_transactions(text)
                for r in result:
                    if r["violations"]:
                        click.echo(dict(transaction=r))

    except Exception as e:
        click.echo(f"{e}")
