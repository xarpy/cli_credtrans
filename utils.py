# -*- coding: utf-8 -*-

from dateutil.parser import parse
from ast import literal_eval


def convert_input(bigfile):
    """
    Converted IO object, to list of dictionary
    """
    try:
        result = list()
        for value in bigfile:
            line = literal_eval(value)
            result.append(line["transaction"])
        return result
    except Exception as e:
        return e


def check_all_transactions(data):
    """
    Check if any violtions and return list to all of them,
    based transaction id.
    """
    try:
        result = list()
        older = None
        for d in data:
            transaction = dict(id=d["id"], violations=[])
            if older is None:
                older = d
            check_score = d["score"] < 200
            lower_installments = d["installments"] < 6
            if check_score:
                transaction["violations"].append("low-score")
            if lower_installments:
                transaction["violations"].append("minimum-installments")
            if older["id"] != d["id"]:
                diff = parse(older["time"]) - parse(d["time"])
                minutes = diff.total_seconds() / 60
                older = d
                if minutes <= 2:
                    transaction["violations"].append("double-transactions")
            if d["installments"] > 0:
                value_installments = d["requested_value"] / d["installments"]
                percentual_income = d["income"] * 0.3
                if value_installments > percentual_income:
                    transaction["violations"].append("compromised-income")
            result.append(transaction)
        return result
    except Exception as e:
        return e
